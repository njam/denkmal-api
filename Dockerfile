FROM node:12-alpine

ENV appdir /home/nodejs/app

RUN mkdir -p ${appdir}
WORKDIR ${appdir}

COPY package.json ${appdir}
COPY yarn.lock ${appdir}

RUN yarn install

COPY . ${appdir}

EXPOSE 5000

CMD ["yarn", "serve"]
