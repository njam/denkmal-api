import {EntityRepository, getCustomRepository, Repository} from "typeorm";
import {Event} from "../entities/event";
import {VenueRepository} from "./venueRepository";
import {Moment} from "moment-timezone";
import {Region} from "../entities/region";
import {Venue} from "../entities/venue";

@EntityRepository(Event)
export class EventRepository extends Repository<Event> {

    async findByEventDay(eventDay: Moment, region: Region, venue: Venue = null, withHidden: boolean = false) {
        const query = this
            .createQueryBuilder("event")
            .leftJoinAndSelect("event.activeVersion", "activeVersion");

        query.andWhere('event.regionId = :regionId', {regionId: region.id});

        if (!withHidden) {
            query.andWhere('event.isHidden = false');
        }

        if (venue) {
            query.andWhere("event.venue_id = :id", {id: venue.id});
        }

        if (eventDay) {
            const [fromDate, untilDate] = region.getEventDayRange(eventDay);

            query.andWhere("activeVersion.from >= :fromDate", {fromDate: fromDate.toDate()});
            query.andWhere("activeVersion.from <= :untilDate", {untilDate: untilDate.toDate()});
        }

        query.innerJoinAndSelect("event.venue", "venue");
        query.innerJoinAndSelect("venue.region", "region");
        query.leftJoinAndSelect("activeVersion.genres", "genres");
        query.leftJoinAndSelect("genres.category", "category");
        query.addSelect(`LOWER(venue.name)`, "lowercasevenue");
        query.addOrderBy("event.isHidden", "ASC");
        query.addOrderBy("lowercasevenue", "ASC");


        const events = await query
            .getMany();

        return events;
    }

    async findSimilar(venueId: string, from: Moment, exactDateMatching: Boolean = false): Promise<Event[]> {
        const venueRepository = getCustomRepository(VenueRepository);
        const venue = await venueRepository.findOne(venueId);
        if (!venue) {
            return null;
        }

        const region = await venue.region;
        const [dayStart, dayEnd] = region.getEventDayRangeIncluding(from);

        const eventRepository = this.createQueryBuilder("event");

        let query = eventRepository.innerJoinAndSelect("event.activeVersion", "activeVersion")
            .andWhere("event.venue_id = :venueId", {venueId: venueId});
        if (exactDateMatching) {
            query.andWhere("activeVersion.from = :fromDate", {fromDate: from});
        } else {
            query.andWhere("activeVersion.from BETWEEN :dayStart AND :dayEnd",
                {dayStart: dayStart.toISOString(), dayEnd: dayEnd.toISOString()})
        }

        query.addSelect('(abs(extract(epoch from activeVersion.from) - ' + from.unix() + '))', "from_diff")
        query.orderBy("from_diff", "ASC");

        return await query.getMany();
    }
}