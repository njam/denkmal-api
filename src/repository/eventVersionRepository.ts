import {EntityRepository, Repository} from "typeorm";
import {EventVersion} from "../entities/eventVersion";

@EntityRepository(EventVersion)
export class EventVersionRepository extends Repository<EventVersion> {

}