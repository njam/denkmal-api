import {EntityRepository, Repository} from "typeorm";
import {Region} from "../entities/region";

@EntityRepository(Region)
export class RegionRepository extends Repository<Region> {

}