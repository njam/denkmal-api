import {Region} from "../entities/region";
import {connectDatabaseTests} from "../database/database";
import {getConnection, getCustomRepository, getRepository} from "typeorm";
import {Venue} from "../entities/venue";
import {Event} from "../entities/event";
import {EventVersion, SourceTypeEnum} from "../entities/eventVersion";
import moment = require("moment-timezone");
import {EventRepository} from "./eventRepository";

describe("eventRepository", async () => {
    beforeEach(async () => {
        await connectDatabaseTests();

        let region = new Region();
        region.slug = "eventRepository-test";
        region.name = "test region";
        region.timeZone = "Europe/Zurich";
        region.dayOffset = 5;
        region.latitude = 0;
        region.longitude = 0;
        region.email = "asdf@asd.com";
        await getConnection().manager.save(region);
    });

    afterEach(async () => {
        await getConnection().close();
    });

    test("findSimilar", async () => {
        const region = await getRepository(Region).findOneOrFail();

        const manager = getConnection().manager;
        const venue1 = new Venue();
        venue1.name = "my venue";
        venue1.region = Promise.resolve(region);
        await manager.save(venue1);

        const event1 = new Event();
        event1.venue = Promise.resolve(venue1);
        await manager.save(event1);

        const version1 = new EventVersion();
        version1.description = "one";
        version1.sourceType = SourceTypeEnum.Admin;
        version1.event = Promise.resolve(event1);
        version1.from = moment.tz("2019-01-31 22:00", "UTC");
        await manager.save(version1);

        event1.activeVersion = version1;
        await manager.save(event1);

        const eventRepository = await getCustomRepository(EventRepository);

        expect(await eventRepository.findSimilar(venue1.id, version1.from)).toHaveLength(1);
        expect(await eventRepository.findSimilar(venue1.id, moment("2019-01-31 19:00"))).toHaveLength(1);
        expect(await eventRepository.findSimilar(venue1.id, moment("2019-01-31 22:30"))).toHaveLength(1);
        expect(await eventRepository.findSimilar(venue1.id, moment("2019-01-30 12:00"))).toHaveLength(0);
        expect(await eventRepository.findSimilar(venue1.id, moment("2019-01-30 12:00"))).toHaveLength(0);
        expect(await eventRepository.findSimilar(venue1.id, moment("2019-02-01 12:00"))).toHaveLength(0);


        // same day, different time
        const event2 = new Event();
        event2.venue = Promise.resolve(venue1);
        await manager.save(event2);

        const version2 = new EventVersion();
        version2.description = "two";
        version2.sourceType = SourceTypeEnum.Admin;
        version2.event = Promise.resolve(event2);
        version2.from = moment.tz("2019-01-31 22:01", "UTC");
        await manager.save(version2);

        event2.activeVersion = version2;
        await manager.save(event2);


        // check ordering
        const later = await eventRepository.findSimilar(venue1.id, moment("2019-01-31 23:00"));
        expect(later).toHaveLength(2);
        expect(later[0].id).toEqual(event2.id);

        const earlier = await eventRepository.findSimilar(venue1.id, moment("2019-01-31 18:00"));
        expect(earlier).toHaveLength(2);
        expect(earlier[0].id).toEqual(event1.id);
    });
});