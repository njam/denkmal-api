import {ValueTransformer} from "typeorm/decorator/options/ValueTransformer";
import * as moment from "moment-timezone";

export class DateTransformer implements ValueTransformer {
    to(value: any) {
        if (!value) {
            return undefined;
        }
        // from moment to timestamp
        const mo = value as moment.Moment;
        return mo.toISOString();
    }

    from(value: any) {
        // from timestamp to moment
        if (!value) {
            return null;
        }
        const mo = moment(value);
        return mo;
    }
}

export const sharedDateTransformer = new DateTransformer();