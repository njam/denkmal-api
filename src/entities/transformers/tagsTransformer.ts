import {ValueTransformer} from "typeorm/decorator/options/ValueTransformer";

export class TagsTransformer implements ValueTransformer {
    to(value: any) {
        return value;
    }

    from(value: any) {
        if (value === null) {
            return [];
        }
        return value;
    }
}

export const sharedTagsTransformer = new TagsTransformer();