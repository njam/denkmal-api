import {EventVersion} from "./eventVersion";
import * as moment from "moment-timezone";

describe("eventVersion", async () => {
    let eventVersion1;
    let fromDate = moment().subtract(5, "hours");
    let untilDate = moment();

    beforeAll(async () => {
        eventVersion1 = Object.assign(new EventVersion(), {
            eventId: "E752D17C-45AA-483C-9395-9417317043C2",
            from: fromDate,
            until: untilDate,
            description: "test",
            genres: ["tag1", "tag2"],
            links: [{ label: "label", url: "url" }]
        });
    });

    test("isSame function", async () => {
        let eventVersion2 = Object.assign(new EventVersion(), {
            eventId: "E752D17C-45AA-483C-9395-9417317043C2",
            from: fromDate,
            until: untilDate,
            description: "test",
            genres: ["tag1", "tag2"],
            links: [{ label: "label", url: "url" }]
        });
        expect(eventVersion1.isSame(eventVersion2)).toBeTruthy();
        eventVersion2.genres.push("tag3");
        expect(eventVersion1.isSame(eventVersion2)).toBeFalsy();
    });
});
