import {Venue} from "entities/venue";
import {Event} from "entities/event";
import {AbstractApiEvent} from "./abstractApiEvent";
import {ApiEventVariant} from "./apiEventVariant";
import {SourceTypeEnum} from "../../entities/eventVersion";

export class ApiEvent extends AbstractApiEvent {
    isHidden: boolean;
    hasTime: boolean;
    isPromoted: boolean;
    venueId: string;
    regionId: string;
    eventDay: string;
    sourceType: SourceTypeEnum;
    sourceIdentifier: string;
    sourceUrl: string;

    // internal
    event: Event;

    async venue() : Promise<Venue> {
        return this.event.venue;
    }

    async variants() : Promise<ApiEventVariant[]> {
        const versions = await this.event.versions;

        var variantsArray = [] as [ApiEventVariant];
        for (var versionI = 0; versionI < versions.length; versionI++) {
            const v = versions[versionI];

            if (v.id != this.event.activeVersionId) {
                const apiEventVariant = await ApiEventVariant.apiEventVariantForEventVersion(v, this.event);
                variantsArray.push(apiEventVariant);
            }
        }

        return variantsArray;
    }

    static async apiEventForEvent(event: Event) : Promise<ApiEvent>  {
        let region = await (await event.venue).region;
        let timeZone = region.timeZone;
        const activeVersion = event.activeVersion;
        const apiEvent = new ApiEvent();
        apiEvent.id = event.id;
        apiEvent.venueId = event.venueId;
        apiEvent.regionId = event.regionId;
        apiEvent.createdAt = event.createdAt;
        apiEvent.from = activeVersion.from.tz(timeZone);
        apiEvent.eventDay = region.getEventDay(activeVersion.from);
        apiEvent.until = activeVersion.until ? activeVersion.until.tz(timeZone) : null;
        apiEvent.description = activeVersion.description;
        apiEvent.isReviewPending = activeVersion.isReviewPending;
        apiEvent.hasTime = activeVersion.hasTime;
        apiEvent.isHidden = event.isHidden;
        apiEvent.isPromoted = event.isPromoted;
        apiEvent.event = event;
        apiEvent.links = activeVersion.links;
        apiEvent.genres = activeVersion.genres;
        apiEvent.tags = activeVersion.tags;
        apiEvent.sourceType = activeVersion.sourceType;
        apiEvent.sourceIdentifier = activeVersion.sourceIdentifier;
        apiEvent.sourceUrl = activeVersion.sourceUrl;
        return apiEvent;
    }
}