import {connectDatabaseTests} from "../database/database";
import {ApolloServer, gql} from "apollo-server-koa";
import {typeDefs} from "./typeDefs";
import {resolvers} from "./resolvers";
import {createTestClient} from "apollo-server-testing";
import {getConnection, getCustomRepository} from "typeorm";
import {RegionRepository} from "../repository/regionRepository";
import {Region} from "../entities/region";
import {VenueRepository} from "../repository/venueRepository";
import {Venue} from "../entities/venue";
import {Event} from "../entities/event";
import {EventVersion, SourceTypeEnum} from "../entities/eventVersion";
import {EventRepository} from "../repository/eventRepository";
import {EventVersionRepository} from "../repository/eventVersionRepository";
import moment = require("moment-timezone");

describe("resolvers test", async () => {
    let query, mutate;

    beforeEach(async () => {
        await connectDatabaseTests();
        const apolloServer = new ApolloServer({typeDefs, resolvers});
        ({query, mutate} = createTestClient(apolloServer));

        const regionRepository = getCustomRepository(RegionRepository);
        const venueRepository = getCustomRepository(VenueRepository);
        const eventRepository = getCustomRepository(EventRepository);
        const eventVersionRepository = getCustomRepository(EventVersionRepository);

        for (let i = 1; i <= 2; i++) {
            const r = new Region();
            r.name = "basel." + i;
            r.slug = r.name;
            r.latitude = 2.2;
            r.longitude = 3.1;
            r.email = "jo@mail.com";
            r.timeZone = "Europe/Zurich";
            r.dayOffset = 5;

            await
                regionRepository.save(r);

            for (let i = 1; i <= 2; i++) {
                const v = new Venue();
                v.region = Promise.resolve(r);
                v.name = r.slug + "-venue-" + i;

                await venueRepository.save(v);

                for(let j = 1; j <= 2; j++) {
                    const rawEvent = new Event();
                    rawEvent.venue = Promise.resolve(v);
                    rawEvent.isHidden = false;
                    rawEvent.regionId = r.id;
                    const event = await eventRepository.save(rawEvent);

                    const version = new EventVersion();
                    version.description = v.name + "-event-" + j;
                    version.from = moment("2040-12-04T19:30:00.000Z").add(j, 'minutes').add(i, 'seconds');
                    version.sourceType = SourceTypeEnum.Admin;
                    version.event = Promise.resolve(event);
                    version.isReviewPending = false;
                    await eventVersionRepository.save(version);
                    console.log("saved!");

                    event.activeVersion = version;
                    await eventRepository.save(event);
                }
            }
        }
    });

    afterEach(async () => {
        await getConnection().close();
    });

    test("Test hierarchies", async () => {
        const regionsQuery = await query({
            query: gql`
                query {
                    regions{
                        name
                        venues {name}
                        events(eventDays:["2040-12-04"]){
                            description
                            from
                            until
                            description
                            venue{
                                region {
                                    name
                                }
                            }
                        }
                    }
                }
            `
        });
        expect(regionsQuery).toMatchSnapshot("regions");
    })
});