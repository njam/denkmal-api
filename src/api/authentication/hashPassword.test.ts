import {checkPassword, hashPassword} from "./hashPassword";

describe("test password hashing", async () => {
    test('hash and verify', async() => {
        const password = "password123"
        const hash = hashPassword(password)
        expect(hash).toBeTruthy()

        expect(checkPassword("bla", hash)).toBeFalsy()
        expect(checkPassword(password, hash)).toBeTruthy()
    })
})