import {AccessLevelEnum} from "../../entities/user";
import {ForbiddenError} from "apollo-server-koa";

export const restrictAccess = function(accessLevel: AccessLevelEnum | Array<AccessLevelEnum>, resolver) {
    let accessLevels = [];
    if (Array.isArray(accessLevel)) {
        accessLevels = accessLevel;
    } else {
        accessLevels = [accessLevel];
    }

    return function(obj, attrs, context, info) {

        if (!context.user || !accessLevels.includes(context.user.accessLevel)) {
            throw new ForbiddenError("no access");
        }

        return resolver(obj, attrs, context, info);
    };
};