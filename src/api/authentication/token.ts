import {User} from "../../entities/user";
import * as jwt from "jsonwebtoken";
import {getManager} from "typeorm";

const privateKey = () : String => {
    if (process.env.ENCRYPTION_KEY) {
        return process.env.ENCRYPTION_KEY
    }
    if (process.env.NODE_ENV === "development" || process.env.NODE_ENV === "test") {
        return "key"
    }
    throw new Error("ENCRYPTION_KEY environment variable must be set")
}

export const createToken = async function(user: User) : Promise<String> {
    const region = await user.region;
    return jwt.sign({
        userId: user.id,
        loginKey: user.loginKey,
        regionSlug: region ? region.slug : null,
        name: user.name,
        accessLevel: user.accessLevel
    }, privateKey());
}

export const getUserWithToken = async (token: String) : Promise<User> => {
    if (!jwt.verify(token, privateKey())) {
        return Promise.reject("invalid token");
    }
    const decoded = jwt.decode(token);
    const userId = decoded.userId;
    const loginKey = decoded.loginKey;

    const entityManager = getManager();
    const user = await entityManager.findOne(User, { id: userId })

    if (user.loginKey !== loginKey) {
        return Promise.reject("wrong login key");
    }

    return Promise.resolve(user);
}