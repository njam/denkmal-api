import {getConnection, getCustomRepository, getManager} from 'typeorm';
import {Venue} from 'entities/venue';
import {Region} from "entities/region";
import {VenueAlias} from "../../../entities/venueAlias";
import {VenueRepository} from "../../../repository/venueRepository";


export const addVenueMutation = async function (_, attrs) {
    const venueRepository = getCustomRepository(VenueRepository);
    const entityManager = getManager();
    const region = await entityManager.findOneOrFail(Region, {id: attrs.regionId});

    if (await venueRepository.findByNameOrAlias(region, attrs.name)) {
        throw new Error(`Duplicate venue name or alias ${attrs.name}`);
    }

    const venue = {
        ...attrs,
        region: Promise.resolve(region)
    }

    return await entityManager.save(Object.assign(new Venue(), venue));
};