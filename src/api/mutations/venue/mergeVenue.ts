import {getCustomRepository} from "typeorm";
import {VenueRepository} from "../../../repository/venueRepository";

export const mergeVenueMutation = async function (_, {fromId, toId}) {
    const venueRepository = getCustomRepository(VenueRepository);

    await venueRepository.mergeVenue(fromId, toId);

    return await venueRepository.findOne({id: toId}, {loadEagerRelations: true});
};