import {getCustomRepository, getManager} from "typeorm";
import {Venue} from "entities/venue";
import {VenueAlias} from "../../../entities/venueAlias";
import {VenueRepository} from "../../../repository/venueRepository";

export const changeVenueMutation = async function (_, attrs) {
    const venueRepository = getCustomRepository(VenueRepository);
    const entityManager = getManager();
    const venueId = attrs.id;
    const venue = await venueRepository.findOneOrFail({id: venueId});

    if (venue.name != attrs.name) {
        if (await venueRepository.findByNameOrAlias(await venue.region, attrs.name)) {
            throw new Error(`Duplicate venue name or alias ${attrs.name}`);
        }
    }

    if (attrs.aliases) {
        const aliasesSet = new Set(venue.aliases.map(a => a.name));
        const newAliases : string[] = attrs.aliases;
        const newAliasesSet = new Set(newAliases);

        // remove removed aliases
        const removed = new Set(Array.from(aliasesSet).filter(x => !newAliasesSet.has(x)));
        for (let name of Array.from(removed)) {
            await entityManager.delete(VenueAlias,{regionId: venue.regionId, name: name});
        }
        const added = new Set(Array.from(newAliasesSet).filter(x => !aliasesSet.has(x)));
        for (let name of Array.from(added)) {
            if (await venueRepository.findByNameOrAlias(await venue.region, name)) {
                throw new Error(`Duplicate venue name or alias ${name}`);
            }
        }
        for (let name of Array.from(added)) {
            await entityManager.save(Object.assign(new VenueAlias(), {
                name: name,
                venue: await entityManager.findOneOrFail(Venue, {id: venueId})
            }));
        }
    }

    const attrsWithoutAliases = {
        ...attrs
    };
    if (attrsWithoutAliases.aliases) {
        delete attrsWithoutAliases.aliases;
    }

    await entityManager.save(Venue, {
        ...attrsWithoutAliases,
        isReviewPending: false
    });

    // refetch the venue to get all the relations
    return await venueRepository.findOneOrFail({id: venueId});
};