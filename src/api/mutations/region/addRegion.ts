import {getManager} from 'typeorm';
import {Region} from 'entities/region';


export const addRegionMutation = async function (_, attrs) {
    const entityManager = getManager();
    const region = {
        ...attrs,
    }

    return await entityManager.save(Object.assign(new Region(), region));
};
