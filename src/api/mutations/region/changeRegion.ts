import {getManager} from 'typeorm';
import {Region} from 'entities/region';


export const changeRegionMutation = async function (_, attrs) {
        const entityManager = getManager();
        const regionId = attrs.id;
        const region = await entityManager.findOneOrFail(Region, {id: regionId});

        return await entityManager.save(Region, {
            ...region,
            ...attrs
        });
    };
