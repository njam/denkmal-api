import {getCustomRepository, getManager, getRepository} from "typeorm";
import {Event} from 'entities/event';
import {Venue} from 'entities/venue';
import {Region} from 'entities/region';
import {EventRepository} from "repository/eventRepository";
import {ApiEvent} from "api/helpers/apiEvent";
import {EventVersionRepository} from "repository/eventVersionRepository";
import {EventVersion, SourceTypeEnum} from "entities/eventVersion";
import {VenueRepository} from "../../../repository/venueRepository";
import {RegionRepository} from "../../../repository/regionRepository";
import {unknownGenreCategoryName} from "../../../entities/genreCategory";
import {Genre} from "../../../entities/genre";
import {sendMail} from "../../../mail/sendMail";
import moment = require("moment");


export const suggestEventMutation = async function (_, attrs) {
    const region = await getCustomRepository(RegionRepository).findOneOrFail({slug: attrs.regionSlug}, {relations: ["genreCategories", "genreCategories.genres"]});

    const venueRepository = getCustomRepository(VenueRepository);
    let venue = await venueRepository.findByNameOrAlias(region, attrs.venueName);

    if (!venue) {
        venue = await getManager().save(Object.assign(new Venue(), {
            name: attrs.venueName,
            isReviewPending: true,
            region: Promise.resolve(region)
        }));
    }

    const eventRepository = getCustomRepository(EventRepository);
    let event = await eventRepository.save(Object.assign(new Event(), {
        isHidden: true,
        venue: Promise.resolve(venue)
    }));

    let genreRelations = [];
    if (attrs.genres) {
        const genreRepository = getRepository(Genre);

        for (let g of attrs.genres) {
            const lowercaseGenre = g.toLowerCase().trim();

            if (genreRelations.find(g => g.name === lowercaseGenre)) {
                continue;
            }

            const foundGenre = await genreRepository.createQueryBuilder('genre')
                .leftJoinAndSelect("genre.category", "category")
                .leftJoinAndSelect("category.region", "region")
                .andWhere("region.id = :regionId", {regionId: region.id})
                .andWhere("genre.name = :genreName", {genreName: lowercaseGenre})
                .getOne();

            if (foundGenre) {
                genreRelations.push({id: foundGenre.id, name: foundGenre.name});
            } else {
                let unknownGenreCategory = (await region).genreCategories.find(c => c.name == unknownGenreCategoryName);

                const genre = new Genre();
                genre.name = lowercaseGenre;
                genre.category = unknownGenreCategory;
                genre.isReviewPending = true;
                await genreRepository.save(genre);

                genreRelations.push({id: genre.id, name: genre.name});
            }
        }
    }

    // create version
    const versionRepository = getCustomRepository(EventVersionRepository);
    const version = await versionRepository.save(Object.assign(new EventVersion(), {
        ...attrs,
        isReviewPending: true,
        sourceType: SourceTypeEnum.Suggestion,
        sourceIdentifier: "Suggestion",
        event: Promise.resolve(event),
        genres: genreRelations
    }));

    event.activeVersion = version;
    await eventRepository.save(event);

    await sendSuggestEventNotificationMail(version, region);

    // convert to api event
    return ApiEvent.apiEventForEvent(event);
};

const sendSuggestEventNotificationMail = async (eventVersion: EventVersion, region: Region) => {
    if (eventVersion.from.clone().subtract(2, 'days').isAfter(moment())) {
        // ignore events suggested for more than two days in the futures
        return;
    }

    const regionEmails = (await region.users).map(u => u.email);
    const event = await eventVersion.event;
    const venue = await event.venue;

    const dateFormat = 'DD.MM.YYYY H:mm';
    const adminUrl = process.env.ADMIN_URL ? process.env.ADMIN_URL : "https://admin.denkmal.org";

    const message = `
    venue: ${venue.name}
    description: ${eventVersion.description}
    from: ${eventVersion.from.format(dateFormat)}
    until: ${eventVersion.until ? eventVersion.until.format(dateFormat) : ""}
    genres: ${eventVersion.genres.map(g => g.name).join(",")}
    links: ${eventVersion.links.map(l => l.label + ": " + l.url).join(",")}
    
    ${adminUrl}/r/${region.slug}/events/${event.id}
    `;

    sendMail(message, `New event suggestion for ${region.name}`, regionEmails);
}