import {getCustomRepository, getManager} from "typeorm";
import {Event} from 'entities/event';
import {Venue} from 'entities/venue';
import {EventRepository} from "repository/eventRepository";
import {ApiEvent} from "api/helpers/apiEvent";
import {EventVersionRepository} from "repository/eventVersionRepository";
import {EventVersion, SourceTypeEnum} from "entities/eventVersion";


export const addEventMutation = async function(_, attrs) {
    const entityManager = getManager();
    const venue = await entityManager.findOneOrFail(Venue, {id: attrs.venueId});

    const eventRepository = getCustomRepository(EventRepository);

    let event = await eventRepository.save(Object.assign(new Event(), {
        isHidden: attrs.isHidden,
        isPromoted: attrs.isPromoted,
        venue: Promise.resolve(venue)
    }));


    let genresObject = {};
    if (attrs.genres) {
        genresObject = {
            genres: attrs.genres.map(genreId => {return {id: genreId}})
        };
    }

    // create version
    const versionRepository = getCustomRepository(EventVersionRepository);
    const version = await versionRepository.save(Object.assign(new EventVersion(), {
        ...attrs,
        isReviewPending: false,
        sourceType: SourceTypeEnum.Admin,
        event: Promise.resolve(event)
    }, genresObject
    ));

    event.activeVersion = version;
    const savedEvent = await eventRepository.save(event);

    // get event with all eager loaded data
    const query = eventRepository.createQueryBuilder("event")
        .where({id: savedEvent.id})
        .innerJoinAndSelect("event.activeVersion", "activeVersion")
        .leftJoinAndSelect("activeVersion.genres", "genres")
        .leftJoinAndSelect("genres.category", "category");

    // convert to api event
    return ApiEvent.apiEventForEvent(await query.getOne());
};
