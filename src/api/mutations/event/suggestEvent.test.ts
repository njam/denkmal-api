import {createTestServer} from "../../../test/testServer";
import {getConnection, getCustomRepository} from "typeorm";
import {gql} from 'apollo-server-koa'
import {EventRepository} from "../../../repository/eventRepository";
import {AccessLevelEnum} from "../../../entities/user";
import moment = require("moment-timezone");

describe("suggestEvent", async () => {
    let query, mutate;
    let regionSlug = "ser1";

    beforeEach(async () => {
        ({query, mutate} = await createTestServer(AccessLevelEnum.Admin));

        await mutate({
            mutation: gql`
                mutation {addRegion(
                    name: "Suggest region"
                    slug: "${regionSlug}"
                    latitude: 40
                    longitude: 50
                    timeZone: "Europe/Zurich"
                    dayOffset: 5
                    email: "mail@asdf.ch"
                ){slug}}
            `
        });
    });

    afterEach(async () => {
        await getConnection().close();
    });

    let getEvents = () => {
        const repository = getCustomRepository(EventRepository);
        const query = repository.createQueryBuilder("event")
            .innerJoinAndSelect("event.activeVersion", "activeVersion")
            .leftJoinAndSelect("activeVersion.genres", "genres")
            .leftJoinAndSelect("genres.category", "category");
        return query.getMany();
    };

    test("suggest an event", async () => {
        let fromDate = moment.tz("Europe/Zurich").add(2, 'days');

        await mutate({
            mutation: gql`
                mutation {suggestEvent(
                    description: "Hey"
                    regionSlug: "${regionSlug}"
                    venueName: "the venue"
                    from: "${fromDate.toISOString()}"
                    genres: ["bla", "blu"]
                ){id}}
            `
        });

        {
            const events = await getEvents();
            expect(events).toHaveLength(1);
            const event = events[0];
            expect(event).toBeDefined();
            expect(event.activeVersion).toBeDefined();
            expect(event.activeVersion.genres).toHaveLength(2);
        }

        await mutate({
            mutation: gql`
                mutation {suggestEvent(
                    description: "Hey jo"
                    regionSlug: "${regionSlug}"
                    venueName: "another venue"
                    from: "${fromDate.toISOString()}"
                    genres: ["bla", "blu"]
                ){id}}
            `
        });

        {
            const events = await getEvents();
            expect(events).toHaveLength(2);
        }
    });
});