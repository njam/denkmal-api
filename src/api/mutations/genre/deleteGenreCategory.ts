import {getManager} from "typeorm";
import {GenreCategory} from "../../../entities/genreCategory";

export const deleteGenreCategoryMutation = async function (_, attrs) {
    const entityManager = getManager();
    const genreCategoryId = attrs.id;
    await entityManager.delete(GenreCategory, {id: genreCategoryId});
    return true;
};