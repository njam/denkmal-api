import {getManager} from 'typeorm';
import {Region} from 'entities/region';
import {GenreCategory} from "../../../entities/genreCategory";


export const addGenreCategoryMutation = async function (_, attrs) {
    const entityManager = getManager();
    const region = await entityManager.findOneOrFail(Region, {id: attrs.regionId});

    const genreCategory = {
        ...attrs,
        region: Promise.resolve(region)
    };

    return await entityManager.save(Object.assign(new GenreCategory(), genreCategory));
};
