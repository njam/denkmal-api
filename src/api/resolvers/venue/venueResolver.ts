import {getCustomRepository} from "typeorm";
import {VenueRepository} from "repository/venueRepository";


export const venueResolver = async function (obj, {id}, context, info) {
    const repository = getCustomRepository(VenueRepository);
    return await repository.findOneOrFail(id);
};
