import {GraphQLScalarType, Kind} from "graphql";
import moment = require("moment-timezone");


export const dateResolver = new GraphQLScalarType({
    name: 'Date',
    description: 'ISO 8601 date string',
    serialize(value) {
        return value.format();
    },
    parseValue(value) {
        const m =  moment.utc(value as string, moment.ISO_8601);
        if (m && m.isValid()) {
            return m;
        }
    },
    parseLiteral(ast) {
        switch (ast.kind) {
            case Kind.STRING:
                const m =  moment(ast.value as string, moment.ISO_8601);
                if (m && m.isValid()) {
                    return m;
                }
        }
    }
});