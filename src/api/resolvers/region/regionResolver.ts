import {RegionRepository} from "repository/regionRepository";
import {getCustomRepository} from "typeorm";

export const regionResolver = async function (obj, {id, slug}, context, info) {
    if (!id && !slug) {
        throw new Error("id or slug parameter required");
    }
    let options = {};
    if (slug) {
        options['where'] = {slug: slug};
    }

    const regionRepository = getCustomRepository(RegionRepository);
    return regionRepository.findOneOrFail(id, options);
};