import {getRepository} from "typeorm";
import {Region} from "../../../entities/region";
import {FieldMap, queryBuilderWithListOptions} from "../../helpers/listOptionsToFindOptions";
import {Genre} from "../../../entities/genre";
import {unknownGenreCategoryName} from "../../../entities/genreCategory";

export const genresListResolver = async function (region : Region, {listOptions, ignoreUnknown, search}, context, info) {
    const repository = getRepository(Genre);

    const alias = "genre";

    const fieldMap: FieldMap = {
        "category.name": "category.name"
    };

    const listQuery = queryBuilderWithListOptions(repository, alias, listOptions, [], fieldMap);
    listQuery.innerJoinAndSelect(alias + ".category", "category");
    listQuery.andWhere("category.region = :regionId", {regionId: region.id});

    if (search && search.length > 0) {
        listQuery.andWhere("genre.name ILIKE :search", {search: search});

        if (listOptions.sort && listOptions.sort.field === 'id') {
            // ordering by genre length for filtering (autosuggest)
            listQuery.addSelect('char_length(genre.name)', "genre_length")
            listQuery.orderBy("genre_length", "ASC");
        }
    }
    if (ignoreUnknown) {
        listQuery.andWhere("category.name != :category", {category: unknownGenreCategoryName});
    }

    return {
        genres: listQuery.getMany(),
        count: listQuery.getCount()
    }
};
