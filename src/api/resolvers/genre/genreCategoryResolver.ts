import {getRepository} from "typeorm";
import {GenreCategory} from "../../../entities/genreCategory";

export const genreCategoryResolver = async function (obj, {id}, context, info) {
    const repository = getRepository(GenreCategory);

    return repository.findOneOrFail(id);
};
