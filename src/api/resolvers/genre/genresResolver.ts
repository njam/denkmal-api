import {Genre} from "../../../entities/genre";
import {getRepository} from "typeorm";

export const genresResolver = async function (obj, {ids}, context, info) {
    const repository = getRepository(Genre);

    return repository.findByIds(ids, {relations: ["category"]});
};
