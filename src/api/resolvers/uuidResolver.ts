import {GraphQLScalarType, Kind} from "graphql";
import * as isUUID from 'validator/lib/isUUID';


export const uuidResolver = new GraphQLScalarType({
    name: 'UUID',
    description: 'An RFC 4122 compatible UUID String',
    serialize(value) {
        return value;
    },
    parseValue(value) {
        return value;
    },
    parseLiteral(ast) {
        switch (ast.kind) {
            case Kind.STRING:
                if (isUUID(ast.value)) {
                    return ast.value
                }
        }
    }
});