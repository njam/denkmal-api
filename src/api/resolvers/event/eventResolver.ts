import {getCustomRepository} from "typeorm";
import {ApiEvent} from "api/helpers/apiEvent";
import {EventRepository} from "repository/eventRepository";
import {EntityNotFoundError} from "typeorm/error/EntityNotFoundError";
import {Event} from "../../../entities/event";

export const eventResolver = async function(obj, { id }, context, info) {
    const repository = getCustomRepository(EventRepository);
    const query = repository.createQueryBuilder("event")
        .where({id: id})
        .innerJoinAndSelect("event.activeVersion", "activeVersion")
        .leftJoinAndSelect("activeVersion.genres", "genres")
        .leftJoinAndSelect("genres.category", "category");
    const event = await query.getOne()

    if (!event) {
        throw new EntityNotFoundError(Event,id);
    }

    return ApiEvent.apiEventForEvent(event);
};