import {getRepository} from "typeorm";
import {User} from "../../../entities/user";

export const userResolver = async function (obj, {id}, context, info) {
    const userRespository = getRepository(User);
    return userRespository.findOneOrFail(id);
};