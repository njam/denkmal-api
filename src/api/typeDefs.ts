import {fileLoader, mergeTypes} from "merge-graphql-schemas";
import * as path from "path";

const typesArray = fileLoader(path.join(__dirname, './types/'), {recursive: true});
const typeDefs = mergeTypes(typesArray, {all: true});

export {typeDefs};
