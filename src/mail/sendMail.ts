import * as AWS from 'aws-sdk';
import {SendEmailRequest} from "aws-sdk/clients/ses";
const globalContext:any = global;


export const sendMail = (message: string, subject: string, receivers : string[]) => {
    AWS.config.credentials = {
        accessKeyId: process.env.SES_ACCESS_KEY_ID,
        secretAccessKey: process.env.SES_SECRET_ACCESS_KEY
    };

    AWS.config.update({region: 'eu-west-1'});

    const params : SendEmailRequest = {
        Destination: {
            BccAddresses: receivers
        },
        Message: {
            Body: {
                Text: {
                    Charset: "UTF-8",
                    Data: message
                }
            },
            Subject: {
                Charset: 'UTF-8',
                Data: subject
            }
        },
        Source: process.env.SES_SENDER_EMAIL ? process.env.SES_SENDER_EMAIL : "notifications@denkmal.org"
    };

    const sendPromise = new AWS.SES({apiVersion: '2010-12-01'}).sendEmail(params).promise();


    sendPromise.then(
        function (data) {
            globalContext.logger.debug(`Mail sent ${data.MessageId}`);
        }).catch(
        function (err) {
            globalContext.logger.error(`Mail failed to send: ${err}`);
        });
};